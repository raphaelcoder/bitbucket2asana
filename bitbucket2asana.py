import sys, re, requests, json, pickle, os.path, shutil
from requests.auth import HTTPBasicAuth

class Configuration():

    def __init__(self, filename='config.b2a', backup_filename='config.b2a.bck'):
        current_dir_path = os.path.dirname(__file__)
        self.config_filepath = os.path.join(current_dir_path, filename)
        self.backup_config_filepath = os.path.join(current_dir_path, backup_filename)

    def backup(self):
        try:
            shutil.copy(self.config_filepath, self.backup_config_filepath)
        except IOError:
            pass

    def load(self):
        try:
            config_file = open(self.config_filepath, 'rb')
            try:
                config_dict = pickle.load(config_file)
            except EOFError:
                config_dict = {}
            finally:
                config_file.close()
        except IOError:
            config_dict = {}

        return config_dict

    def save(self, config_dict):
        self.backup()

        config_file = open(self.config_filepath, 'wb')
        try:
            pickle.dump(config_dict, config_file)
        finally:
            config_file.close()


def write_workspace_id(match, config_dict):
    workspace_id = match.groups()[0]
    config_dict['workspace_id'] = workspace_id

def write_asana_api_key(match, config_dict):
    (bitbucket_username, asana_api_key) = match.groups()
    if not config_dict.has_key('asana_api_keys'):
        config_dict['asana_api_keys'] = {}
    config_dict['asana_api_keys'][bitbucket_username] = asana_api_key

def create_new_task(match, config_dict):

    if not config_dict.has_key('asana_api_keys') or not config_dict.has_key('workspace_id'):
        return

    (subject, repo, issue_number, assignee) = match.groups()
    subject = re.sub(r'[\r\n]+', '', subject, flags=re.M|re.S)
    issue_url = 'https://bitbucket.org/%s/issue/%s/' % (repo, issue_number)
    asana_api_keys = config_dict['asana_api_keys']
    if asana_api_keys.has_key(assignee):
        asana_api_key = asana_api_keys[assignee]
        workspace_id = config_dict['workspace_id']

        r = requests.get('https://app.asana.com/api/1.0/users/me', auth=HTTPBasicAuth(asana_api_key, ''))
        asana_userid = json.loads(r.text)['data']['id']

        r = requests.post('https://app.asana.com/api/1.0/tasks', auth=HTTPBasicAuth(asana_api_key, ''), data=json.dumps({'data':{'workspace':workspace_id, 'assignee':asana_userid, 'name':subject, 'notes':issue_url}}))

actions = {
    'workspace_id_received': {
        'pattern': r'.*asana_workspace_id\s*:\s*(\d+).*',
        'action': write_workspace_id,
    },
    'asana_api_key_received': {
        'pattern': r'.*bitbucket_username\s*:\s*\"([^\"]+)\".*asana_api_key\s*:\s*\"([^\"]+)\".*',
        'action': write_asana_api_key,
    },
    'new_issue_created': {
        'pattern': r'.*Subject:\s*(\[(.*)\].*issue\s#(\d+)[^\n|^\r]+).*Responsible\:\s*([^\n|^\r]+).*',
        'action': create_new_task,
    },
    'existing_issue_assigned': {
        'pattern': r'.*Subject:\s*Re:\s*(\[(.*)\].*issue\s#(\d+)[^\n|^\r]+).*responsible:.*->+\s*([^\n|^\r]+).*',
        'action': create_new_task,
    }
}

def process_email(email_plain_text, config_dict):
    for action in actions:
        action_dict = actions[action]
        pattern_str = action_dict['pattern']
        pattern = re.compile(pattern_str, re.M|re.S)
        match = pattern.match(email_plain_text)
        if match and len(match.groups()) != 0:
            action_dict['action'](match, config_dict)


configuration = Configuration('config.b2a')
config_dict = configuration.load()
process_email(''.join(sys.stdin.readlines()), config_dict)
configuration.save(config_dict)
