Bitbucket2Asana
===============

Bitbucket2Asana is a simple system for automated task creation triggered by Bitbucket issue assignment. In case you are using Asana to manage your projects and tasks, but your team also uses BitBucket Issues as an issue tracker, you will find this tool useful.

It basically works as an email processing script which recognizes when the user receives an issue assignment notification and then creates a task in Asana for him.


Setup
-----

Bitbucket2Asana is written in Python and depends on the [Requests library](https://github.com/kennethreitz/requests). I personally use [WebFaction](http://www.webfaction.com/) and I will be describing a setup done on Webfaction hosting platform, but a similar setup should be possible on any modern hosting environment. 

In order to configure Bitbucket2Asana you need to make sure  
1. that the system knows on which Asana workspace it operates  
2. that the system knows for which users it should create tasks  
3. that the Bitbucket email notifications are forwarded to the system  

To install the system, please follow theses steps:  
1. clone the Bitbucket2Asana repository  
2. make sure that you have Requests library available on Python path (either through virtual environment or through the main Python installation)  
3. modify bitbucket2asana.sh script according to your setup from step 2 (my shell script is making sure that virtualenvwrapper is loaded and then activates the virtualenv before calling the bitbucket2asana.py)  

To configure the system, please follow these steps:  
1. create a new dedicated email address, for example bitbucket2asana@your-domain.com, and make sure that all emails sent to it are forwarded to bitbucket2asana.sh script through standard input (in case of Webfaction, you would use "Send to program" option under Email settings)  
2. send an email to the dedicated email address with the Asana workspace id in the body in the following format:
`asana_workspace_id:<asana_workspace_id>`  
3. send an email to the dedicated email address with your Bitbucket username and your Asana API key in the body in the following format:
`bitbucket_username:"<username>" asana_api_key:"<asana_api_key>"`  
4. every user that wants to have his Bitbucket issues appear in Asana should send the same email from step 3  
5. make sure that all Bitbucket email notifications are forwarded to your dedicated Bitbucket2Asana email address  

And now you have your Bitbucket issues automagically appear as Asana tasks.